import {dirname} from 'path';
import {fileURLToPath} from 'url';
import path from 'path';
import fs from 'fs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
let writeStream = fs.createWriteStream('log.txt');

export function getPath(file = '') {
	const mainPath = path.join(__dirname, 'files');
	const isExistsFolder = fs.existsSync(mainPath);
	if (!isExistsFolder) {
		try {
			fs.mkdirSync(mainPath, {recursive: true});
		} catch (err) {
			throw err;
		}
	}
	return path.join(__dirname, 'files', file);
}

export function addInfo(req, res, next) {
	const info = {
		date: new Date().toLocaleString(),
		method: req.method,
		url: req.url,
		data: req.body,
	};

	writeStream.write(JSON.stringify(info) + '\n');
	next();
}

export function checkParam(req, res, next) {
	const {filename, content} = req.body;
	if (!filename || !filename.trim()) {
		return res.status(400).json({
			message: "Please specify 'filename' parameter",
		});
	} else if (!content || !content.trim()) {
		return res.status(400).json({
			message: "Please specify 'content' parameter",
		});
	}
	next();
}

export function checkExistFile(req, res, next) {
	const file = req.params.name;
	fs.stat(getPath(file), (err, stat) => {
		if (err) {
			return res.status(400).json({
				message: `No file with '${file}' filename found`,
			});
		}
		req.uploadDate = stat.atime.toLocaleString();
		next();
	});
}

export function checkExtention(req, res, next) {
	const supportExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
	const file = req.body.filename;
	const ext = path.extname(file);
	if (!supportExtensions.includes(ext)) {
		return res.status(400).json({
			message: 'Not supported extension',
		});
	}
	next();
}

export async function passwordGuard(req, res, next) {	
	const method = req.method;
	const store = path.resolve(__dirname, '', 'passwords.json');
	try {
		const isExistsFile = fs.existsSync(store);
		if (!isExistsFile) {
			fs.writeFileSync(store, JSON.stringify({}));
		}
	} catch (e) {
		return res.status(500).json({
			message: 'Server error',
		});
	}
	if (
		method === 'POST' &&
		req.query.password 
	) {
		console.log('Ok');
		
		try {
			const passwordData = JSON.parse(fs.readFileSync(store));
			const newpasswordData = {
				...passwordData,
				[req.body.filename]: req.query.password,
			};
			fs.writeFileSync(store, JSON.stringify(newpasswordData));
		} catch (e) {
			return res.status(500).json({
				message: 'Server error',
			});
		}
	} else if (method === 'GET' || method === 'PUTCH' || method === 'DELETE') {
		try {
			const file = req.params.name;
			const passwordData = JSON.parse(fs.readFileSync(store));
			const isProtected = !!passwordData[file];
			const userPassword = req.query.password;

			if (isProtected && !userPassword) {
				return res.status(403).json({
					message: 'This file is protected password is required',
				});
			}

			if (isProtected && passwordData[file] != userPassword) {
				return res.status(403).json({
					message: 'Password is wrong',
				});
			}

			if (method === 'DELETE') {
				delete passwordData[file];
			}
		} catch (e) {
			return res.status(500).json({
				message: 'Server error',
			});
		}
	}
	return next();
}
