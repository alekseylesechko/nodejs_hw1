import dotenv from 'dotenv';
import express from 'express';
import cors from 'cors';
import {router as filesRouter} from './routes/file.js';
import {addInfo, getPath} from './middlewares.js';

dotenv.config();
const app = express();
const PORT = process.env.PORT || 8080;

//Middleware
app.use(cors());
app.use(express.json());
app.use(addInfo);

//Route middlewares
app.use('/api/files', filesRouter);

app.listen(PORT, () => {
	getPath()
	console.log(`Server is running on port ${PORT}`);
});
