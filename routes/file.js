import express from 'express';
import {
	addFile,
	getFileByName,
	getFiles,
	deleteFileByName,
	changeFileByName,
} from '../controllers/file.js';
import {
	checkExistFile,
	checkExtention,
	checkParam,
	passwordGuard,
} from '../middlewares.js';

export const router = express.Router();

//GET api/files
router.get('/', getFiles);

//POST api/files
router.post('/', checkParam, checkExtention, passwordGuard, addFile);

//GET api/files/:filename
router.get('/:name', checkExistFile, passwordGuard, getFileByName);

//PATCH api/files/:filename
router.patch('/:name', checkExistFile, passwordGuard, changeFileByName);

//DELETE api/files/:filename
router.delete('/:name', checkExistFile, passwordGuard, deleteFileByName);
