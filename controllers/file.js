import {getPath} from '../middlewares.js';
import fs from 'fs';
import path from 'path';

export const getFiles = (req, res) => {
	const store = getPath();

	fs.readdir(store, (err, files) => {
		if (err) {
			return res.status(500).json({
				message: 'Server error',
			});
		}
		if (!files.length) {
			return res.status(400).json({message: 'There are no files'});
		}

		return res.status(200).json({
			message: 'Success',
			files,
		});
	});
};

export const getFileByName = (req, res) => {
	const file = req.params.name;

	fs.readFile(getPath(file), (err, data) => {
		if (err) {
			return res.status(500).json({message: 'Server error'});
		}
		return res.json({
			message: 'Success',
			filename: file,
			content: `${data}`,
			extension: path.extname(file).replace('.', ''),
			uploadedDate: req.uploadDate,
		});
	});
};

export const addFile = (req, res) => {
	const {filename, content} = req.body;
	const store = getPath(filename);

	fs.writeFile(store, content, (err) => {
		if (err) {
			return res.status(500).json({
				message: 'Server error',
			});
		}
		return res.status(200).json({
			message: 'File created successfully',
		});
	});
};

export const changeFileByName = (req, res) => {
	const content = req.body.content;
	if (!content) {
		return res.status(400).json({
			message: "Please specify 'content' parameter",
		});
	}
	const filename = req.params.name;
	const store = getPath(filename);

	fs.writeFile(store, content, (err) => {
		if (err) {
			res.status(500).json({
				message: 'Server error',
			});
		}
		return res.status(200).json({
			message: 'File update successfully',
		});
	});
};

export const deleteFileByName = (req, res) => {
	const file = req.params.name;

	fs.rm(getPath(file), (err) => {
		if (err) {
			return res.status(500).json({message: 'Server error'});
		}
		return res.json({
			message: 'successfully deleted',
		});
	});
};
